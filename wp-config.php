<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'excelsior');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '<x*/2:L!|n_a.b<mB=j*a:o1hUBJ^e`#M)zynGc<x&UMrtYaCC$v^K_1s0;L(TCt');
define('SECURE_AUTH_KEY',  'q9,6Nge{]#Bo9fnGdOO[x&Yu$bD[;Cpi)rk21ABob!1k16 ..<Z%.XU+hj iVd>>');
define('LOGGED_IN_KEY',    '1ddViQ(Y@rAjc5-_^#3Rjt;uN0cD+/@tIkeEdMoZOnjp[)hgTSQ^5I3+9NKSWf`o');
define('NONCE_KEY',        '=1v=HY_^G{leSKQh!HBb&u(qeSJ]ssnp+J78&Fw#$OnkZG.2[F(}0y)Hk,cwNw1N');
define('AUTH_SALT',        'SED8);{7v7T(m$KZ]p[&~T-T,]5CAFm$M+Xl4 _eu%`%kSRm5amO>JM?omTCTfPI');
define('SECURE_AUTH_SALT', '$OZv*1#7qEOW-lryws9[5H%mwRnFwcXq5NKcP-];r_OR|z_W4HW+]w@Tit6Mk*W|');
define('LOGGED_IN_SALT',   'TMmpZ7d5wc;ptTiv_bXRchL[+~YOF_wgiOJXcum1Zsw|D*rwT#eQMs37G7 @o*N}');
define('NONCE_SALT',       '7=7A$Uv%}GXe15O,gr8^HToeOcJsGq|^}[P+~/41aL5,AzWvkwX<~>jI|;itSyDF');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
